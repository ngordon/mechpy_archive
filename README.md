
mechpy - a mechanical engineer's toolbox

Mechpy was created for two reasons.
 * To provide the practicing engineer with applications or boilerplate code to quickly replicate and solve real-world systems common in mechanical engineering
 * To give the engineering student a code baes from which to suppliment learning through hand-calculations and an easy way to check work.

There are many different tools available to engineers. Hand-calculations, spreadsheets, and code are all great ways to perform calculations or visualize data or concepts. MATLAB is the defacto tool to solve many engineering calculations, but is just too expensive to be a practical tool for many of us. Octave, Scilab, or Freelab are great alternatives, but is limited in scope to calculation. I began using python for calculations and visualzations and have found it to be a very powerful tool with many existing modules for mathematics and plotting, in addition to the thousands of other libraries for general computing.

TODO
* apparently I am redoing the documentation and examples using doconce.
* clean up stupid links
* check out FEM - Numerical Python pg 690/1311
* add failure criteria to composites and mutli-material
* add composite plate code to composites
* convert matlab code for plotting test data
